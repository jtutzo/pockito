interface DecisionMaker {
  name: string
  hasSigningAuthority: boolean
  hasCreditAuthority: boolean
}

export default DecisionMaker